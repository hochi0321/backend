package com.fsse2203.project_backend.api;

import com.fsse2203.project_backend.Exception.CartItemNotFoundException;
import com.fsse2203.project_backend.Exception.InvalidQuantityException;
import com.fsse2203.project_backend.Exception.ProductNotFoundException;
import com.fsse2203.project_backend.Exception.UserNotFoundException;
import com.fsse2203.project_backend.config.EnvConfig;
import com.fsse2203.project_backend.data.cartItem.AddCartItemData;
import com.fsse2203.project_backend.data.cartItem.CartItemDetailsData;
import com.fsse2203.project_backend.data.cartItem.DeleteCartItemData;
import com.fsse2203.project_backend.data.cartItem.UpdateCartQuantityData;
import com.fsse2203.project_backend.data.cartItem.dto.CartItemDetailsResponseDto;
import com.fsse2203.project_backend.data.cartItem.dto.CartItemResultResponseDto;
import com.fsse2203.project_backend.service.CartService;
import com.fsse2203.project_backend.util.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = {EnvConfig.devBaseUrl, EnvConfig.prodBaseUrl}, maxAge = 3600)
@RestController
@RequestMapping("/cart")
public class CartApi {
    private final CartService cartService;
    @Autowired
    public CartApi(CartService cartService) {
        this.cartService = cartService;
    }

    @PutMapping("/add-item/{pid}/{quantity}")
    public CartItemResultResponseDto addCartItem(@PathVariable Integer pid, @PathVariable Integer quantity, Authentication authentication) throws ProductNotFoundException, InvalidQuantityException, UserNotFoundException {
        String firebaseUid = SecurityUtil.getFirebaseUid(authentication);
        return new CartItemResultResponseDto(cartService.addCartItem(new AddCartItemData(pid, firebaseUid, quantity)));
    }

    @GetMapping("")
    public List<CartItemDetailsResponseDto> getAllCartItem(Authentication authentication) throws UserNotFoundException {
        String firebaseUid = SecurityUtil.getFirebaseUid(authentication);
        List<CartItemDetailsResponseDto> dtos = new ArrayList<>();
        for (CartItemDetailsData data: cartService.getAllCartItem(firebaseUid)) {
            dtos.add(new CartItemDetailsResponseDto(data));
        }
        return dtos;
    }

    @PatchMapping("/{pid}/{quantity}")
    public CartItemDetailsResponseDto updateCartQuantity(@PathVariable Integer pid, @PathVariable Integer quantity,Authentication authentication) throws CartItemNotFoundException, InvalidQuantityException, UserNotFoundException {
        String firebaseUid = SecurityUtil.getFirebaseUid(authentication);
        return new CartItemDetailsResponseDto(cartService.updateCartQuantity(new UpdateCartQuantityData(pid,quantity,firebaseUid)));
    }

    @DeleteMapping("/{pid}")
    public CartItemResultResponseDto deleteCartItem(@PathVariable Integer pid, Authentication authentication) throws CartItemNotFoundException, UserNotFoundException {
        String firebaseUid = SecurityUtil.getFirebaseUid(authentication);
        return new CartItemResultResponseDto(cartService.deleteCartItem(new DeleteCartItemData(pid,firebaseUid)));
    }
}
