package com.fsse2203.project_backend.api;

import com.fsse2203.project_backend.Exception.ProductNotFoundException;
import com.fsse2203.project_backend.config.EnvConfig;
import com.fsse2203.project_backend.data.product.CreateProductData;
import com.fsse2203.project_backend.data.product.ProductDetailsData;
import com.fsse2203.project_backend.data.product.dto.CreateProductRequestDto;
import com.fsse2203.project_backend.data.product.dto.ProductDetailsResponseDto;
import com.fsse2203.project_backend.data.product.dto.ProductIdDetailsResponseDto;
import com.fsse2203.project_backend.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = {EnvConfig.devBaseUrl, EnvConfig.prodBaseUrl}, maxAge = 3600)
@RestController
@RequestMapping("/public")
public class ProductApi {
    private final ProductService productservice;

    @Autowired
    public ProductApi(ProductService productservice) {
        this.productservice = productservice;
    }

    @GetMapping("/product")
    public List<ProductDetailsResponseDto> getAllProduct() {
        List<ProductDetailsResponseDto> dtos = new ArrayList<>();
        productservice.getAllProduct();
        for (ProductDetailsData data : productservice.getAllProduct()) {
            dtos.add(new ProductDetailsResponseDto(data));
        }
        return dtos;
    }

    @GetMapping("/product/{id}")
    public ProductIdDetailsResponseDto getProductByPid(@PathVariable("id") Integer pid) throws ProductNotFoundException {
        return new ProductIdDetailsResponseDto(productservice.getProductDetailsDataById(pid));
    }

    @PostMapping("/product/create")
    public ProductIdDetailsResponseDto createProduct(@RequestBody CreateProductRequestDto createProductRequestDto){
        return new ProductIdDetailsResponseDto(productservice.createProduct(new CreateProductData(createProductRequestDto)));
    }
}
