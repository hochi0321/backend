package com.fsse2203.project_backend.api;

import com.fsse2203.project_backend.Exception.*;
import com.fsse2203.project_backend.config.EnvConfig;
import com.fsse2203.project_backend.data.transaction.dto.TransactionDetailsResponseDto;
import com.fsse2203.project_backend.service.TransactionService;
import com.fsse2203.project_backend.util.SecurityUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = {EnvConfig.devBaseUrl, EnvConfig.prodBaseUrl}, maxAge = 3600)
@RestController
@RequestMapping("/transaction")
public class TransactionApi {
    private final TransactionService transactionService;

    @Autowired
    public TransactionApi(TransactionService transactionService) {
        this.transactionService = transactionService;
    }

    @PostMapping("/prepare")
    public TransactionDetailsResponseDto createTransaction(Authentication authentication) throws ProductNotFoundException, UserNotFoundException, CartItemNotFoundException, TransactionNotFoundException, CartEmptyException {
        String firebaseUid = SecurityUtil.getFirebaseUid(authentication);
        return new TransactionDetailsResponseDto(transactionService.createTransaction(firebaseUid));
    }

    @GetMapping("/{tid}")
    public TransactionDetailsResponseDto getTransactionDetailsById(@PathVariable Integer tid,Authentication authentication) throws ProductNotFoundException, TransactionNotFoundException {
        String firebaseUid = SecurityUtil.getFirebaseUid(authentication);
        return new TransactionDetailsResponseDto(transactionService.getTransactionDetailsByTid(tid,firebaseUid));
    }

    @PatchMapping("/{tid}")
    public TransactionDetailsResponseDto updateTransactionStatus(@PathVariable Integer tid,Authentication authentication) throws ProductNotFoundException, TransactionNotFoundException, StockNotEnoughException {
        String firebaseUid = SecurityUtil.getFirebaseUid(authentication);
        return new TransactionDetailsResponseDto(transactionService.updateTransactionStatus(tid,firebaseUid));
    }

    @PatchMapping("/{tid}/finish")
    public TransactionDetailsResponseDto finishTransaction(@PathVariable Integer tid,Authentication authentication) throws ProductNotFoundException, TransactionNotFoundException {
        String firebaseUid = SecurityUtil.getFirebaseUid(authentication);
        return new TransactionDetailsResponseDto(transactionService.finishTransaction(tid,firebaseUid));
    }
}
