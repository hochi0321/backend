package com.fsse2203.project_backend.api;

import com.fsse2203.project_backend.config.EnvConfig;
import com.fsse2203.project_backend.data.user.UserDetailsData;
import com.fsse2203.project_backend.data.user.dto.UserDetailsResponseDto;
import com.fsse2203.project_backend.service.UserService;
import com.google.firebase.auth.FirebaseToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = {EnvConfig.devBaseUrl, EnvConfig.prodBaseUrl}, maxAge = 3600)
@RestController
public class UserApi {
    private final UserService userService;
    @Autowired
    public UserApi(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/user/login")
    public UserDetailsResponseDto login(Authentication authentication){
       FirebaseToken token = (FirebaseToken)authentication.getPrincipal();
       UserDetailsData loginUser = userService.getUserDetailsDataByFirebaseUid(token.getUid());
       return new UserDetailsResponseDto(loginUser);
    }


}
