package com.fsse2203.project_backend.data.cartItem;

public class AddCartItemData {
    private Integer pid;
    private String firebaseUid;
    private Integer quantity;

    public AddCartItemData(Integer pid, String firebaseUid, Integer quantity) {
        this.pid = pid;
        this.firebaseUid = firebaseUid;
        this.quantity = quantity;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getFirebaseUid() {
        return firebaseUid;
    }

    public void setFirebaseUid(String firebaseUid) {
        this.firebaseUid = firebaseUid;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }
}
