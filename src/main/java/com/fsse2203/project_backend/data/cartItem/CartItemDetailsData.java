package com.fsse2203.project_backend.data.cartItem;

import com.fsse2203.project_backend.data.cartItem.entity.CartItemEntity;

public class CartItemDetailsData {
    private Integer pid;
    private String name;
    private String description;
    private String imageUrl;
    private Double price;
    private Integer cartQuantity;
    private Integer stock;


    public CartItemDetailsData(CartItemEntity entity) {
        this.pid = entity.getProduct().getPid();
        this.name = entity.getProduct().getName();
        this.description = entity.getProduct().getDescription();
        this.imageUrl = entity.getProduct().getImageUrl();
        this.price = entity.getProduct().getPrice();
        this.stock = entity.getProduct().getStock();
        this.cartQuantity = entity.getQuantity();
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getCartQuantity() {
        return cartQuantity;
    }

    public void setCartQuantity(Integer cartQuantity) {
        this.cartQuantity = cartQuantity;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "CartItemDetailsData{" +
                "pid=" + pid +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", price=" + price +
                ", cartQuantity=" + cartQuantity +
                ", stock=" + stock +
                '}';
    }
}
