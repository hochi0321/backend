package com.fsse2203.project_backend.data.cartItem;

public class CartItemResultData {
    private String result = "SUCCESS";

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
