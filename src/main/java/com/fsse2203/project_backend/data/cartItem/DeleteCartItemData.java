package com.fsse2203.project_backend.data.cartItem;

public class DeleteCartItemData {
    private Integer pid;
    private String firebaseUid;

    public DeleteCartItemData(Integer pid, String firebaseUid) {
        this.pid = pid;
        this.firebaseUid = firebaseUid;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getFirebaseUid() {
        return firebaseUid;
    }

    public void setFirebaseUid(String firebaseUid) {
        this.firebaseUid = firebaseUid;
    }
}
