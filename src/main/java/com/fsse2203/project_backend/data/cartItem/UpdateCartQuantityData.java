package com.fsse2203.project_backend.data.cartItem;

public class UpdateCartQuantityData {
    private Integer pid;
    private Integer quantity;
    private String firebaseUid;

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getFirebaseUid() {
        return firebaseUid;
    }

    public void setFirebaseUid(String firebaseUid) {
        this.firebaseUid = firebaseUid;
    }

    public UpdateCartQuantityData(Integer pid, Integer quantity, String firebaseUid) {

        this.pid = pid;
        this.quantity = quantity;
        this.firebaseUid = firebaseUid;
    }
}
