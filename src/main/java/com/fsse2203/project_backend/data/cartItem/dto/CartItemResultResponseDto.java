package com.fsse2203.project_backend.data.cartItem.dto;

import com.fsse2203.project_backend.data.cartItem.CartItemResultData;

public class CartItemResultResponseDto {
    private String result = "SUCCESS";

    public CartItemResultResponseDto(CartItemResultData data) {
        this.result = data.getResult();
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
