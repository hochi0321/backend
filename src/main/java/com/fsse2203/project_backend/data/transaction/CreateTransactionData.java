package com.fsse2203.project_backend.data.transaction;

//import com.fsse2203.project_backend.data.transactionProduct.TransactionProductData;
//import com.fsse2203.project_backend.data.transactionProduct.entity.TransactionProductEntity;
import com.fsse2203.project_backend.data.transactionProduct.TransactionProductData;
import com.fsse2203.project_backend.data.user.UserDetailsData;
import com.fsse2203.project_backend.data.user.entity.UserEntity;

import java.time.LocalDateTime;
import java.util.List;

public class CreateTransactionData {
    private Integer tid;
    private UserDetailsData user;
    private List<TransactionProductData> transactionProduct;
    private LocalDateTime datetime;
    private Status status;
    private Double total;

    public CreateTransactionData(List<TransactionProductData> datas) {
        this.transactionProduct = datas;
        this.datetime = LocalDateTime.now();
        this.status = Status.PREPARE;
        Double sum =0d;
        for (TransactionProductData data: datas) {
            sum += data.getSubtotal();
        }
        this.total = sum;
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public UserDetailsData getUser() {
        return user;
    }

    public void setUser(UserDetailsData user) {
        this.user = user;
    }

    public List<TransactionProductData> getTransactionProduct() {
        return transactionProduct;
    }

    public void setTransactionProduct(List<TransactionProductData> transactionProduct) {
        this.transactionProduct = transactionProduct;
    }

    public LocalDateTime getDatetime() {
        return datetime;
    }

    public void setDatetime(LocalDateTime datetime) {
        this.datetime = datetime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
}
