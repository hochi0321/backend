package com.fsse2203.project_backend.data.transaction;

public enum Status {
        PREPARE,
        PROCESSING,
        SUCCESS
}
