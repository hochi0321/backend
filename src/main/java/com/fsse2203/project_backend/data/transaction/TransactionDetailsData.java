package com.fsse2203.project_backend.data.transaction;

import com.fsse2203.project_backend.data.transaction.entity.TransactionEntity;
import com.fsse2203.project_backend.data.transactionProduct.TransactionProductData;
import com.fsse2203.project_backend.data.transactionProduct.entity.TransactionProductEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class TransactionDetailsData {
    private Integer tid;
    private Integer buyerUid;
    private LocalDateTime datetime;
    private Status status;
    private Double total;
    private List<TransactionProductData> items = new ArrayList<>();

    public TransactionDetailsData(TransactionEntity entity) {
        this.tid = entity.getTid();
        this.buyerUid = entity.getUser().getUid();
        this.datetime = entity.getDatetime();
        this.status = entity.getStatus();
        this.total = entity.getTotal();
        for (TransactionProductEntity transactionProductEntity : entity.getItems()) {
            this.items.add(new TransactionProductData(transactionProductEntity));
        }
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public Integer getBuyerUid() {
        return buyerUid;
    }

    public void setBuyerUid(Integer buyerUid) {
        this.buyerUid = buyerUid;
    }

    public LocalDateTime getDatetime() {
        return datetime;
    }

    public void setDatetime(LocalDateTime datetime) {
        this.datetime = datetime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public List<TransactionProductData> getItems() {
        return items;
    }

    public void setItems(List<TransactionProductData> items) {
        this.items = items;
    }

}
