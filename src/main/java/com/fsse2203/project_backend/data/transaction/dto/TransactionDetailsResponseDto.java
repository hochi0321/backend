package com.fsse2203.project_backend.data.transaction.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fsse2203.project_backend.data.transaction.Status;
import com.fsse2203.project_backend.data.transaction.TransactionDetailsData;
import com.fsse2203.project_backend.data.transactionProduct.TransactionProductData;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class TransactionDetailsResponseDto {
    @JsonProperty("tid")
    private Integer tid;
    @JsonProperty("buyer_uid")
    private Integer buyerUid;
    @JsonProperty("datetime")
    private String datetime;
    @JsonProperty("status")
    private Status status;
    @JsonProperty("total")
    private Double total;
    @JsonProperty("items")
    private List<TransactionProductData> items;

    public TransactionDetailsResponseDto(TransactionDetailsData data) {
        this.tid = data.getTid();
        this.buyerUid = data.getBuyerUid();
        this.datetime = data.getDatetime().format(DateTimeFormatter.ISO_DATE_TIME);
        this.status = data.getStatus();
        this.total = data.getTotal();
        this.items = data.getItems();
    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public Integer getBuyerUid() {
        return buyerUid;
    }

    public void setBuyerUid(Integer buyerUid) {
        this.buyerUid = buyerUid;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public List<TransactionProductData> getItems() {
        return items;
    }

    public void setItems(List<TransactionProductData> items) {
        this.items = items;
    }
}
