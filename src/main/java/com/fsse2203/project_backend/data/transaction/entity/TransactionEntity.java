package com.fsse2203.project_backend.data.transaction.entity;

import com.fsse2203.project_backend.data.cartItem.entity.CartItemEntity;
import com.fsse2203.project_backend.data.transaction.Status;
import com.fsse2203.project_backend.data.transactionProduct.entity.TransactionProductEntity;
import com.fsse2203.project_backend.data.user.entity.UserEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Entity
@Table(name = "Transaction")
public class TransactionEntity {

    @Id
    @Column(name = "tid", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer tid;

    @ManyToOne
    @JoinColumn(name = "uid", nullable = false)
    private UserEntity user;

    @Column(name = "datetime", nullable = false)
    private LocalDateTime datetime;

    @Column(name = "status", nullable = false)
    private Status status;

    @Column(name = "total")
    private Double total;

    @OneToMany(mappedBy="transaction")
    private List<TransactionProductEntity> items;

    public TransactionEntity(UserEntity user, List<CartItemEntity> cartItemEntities) {
        this.user = user;
        this.datetime = LocalDateTime.now().truncatedTo(ChronoUnit.SECONDS);
        this.status = Status.PREPARE;
        setTotal(cartItemEntities);
    }

    public TransactionEntity() {

    }

    public Integer getTid() {
        return tid;
    }

    public void setTid(Integer tid) {
        this.tid = tid;
    }

    public UserEntity getUser() {
        return user;
    }

    public void setUser(UserEntity user) {
        this.user = user;
    }

    public LocalDateTime getDatetime() {
        return datetime;
    }

    public void setDatetime(LocalDateTime datetime) {
        this.datetime = datetime;
    }

    public List<TransactionProductEntity> getItems() {
        return items;
    }

    public void setItems(List<TransactionProductEntity> items) {
        this.items = items;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }
    public void setTotal(List<CartItemEntity> cartItemEntities) {
        this.total = 0.0;
        for (CartItemEntity cartItemEntity: cartItemEntities) {
            this.total += cartItemEntity.getProduct().getPrice() * cartItemEntity.getQuantity();
        }
    }
}
