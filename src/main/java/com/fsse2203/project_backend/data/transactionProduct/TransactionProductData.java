package com.fsse2203.project_backend.data.transactionProduct;

import com.fsse2203.project_backend.data.cartItem.CartItemDetailsData;
import com.fsse2203.project_backend.data.product.ProductDetailsData;
import com.fsse2203.project_backend.data.transactionProduct.entity.TransactionProductEntity;

public class TransactionProductData {
    private Integer tpid;
    private ProductDetailsData product = new ProductDetailsData();
    private Integer quantity;
    private Double subtotal;

    public TransactionProductData(TransactionProductEntity entity) {
        this.tpid = entity.getTpid();
        this.product.setPid(entity.getPid());
        this.product.setName(entity.getName());
        this.product.setDescription(entity.getDescription());
        this.product.setImageUrl(entity.getImageUrl());
        this.product.setPrice(entity.getPrice());
        this.product.setStock(entity.getStock());
        this.quantity = entity.getQuantity();
        this.subtotal = entity.getSubtotal();
    }

    public Integer getTpid() {
        return tpid;
    }

    public void setTpid(Integer tpid) {
        this.tpid = tpid;
    }

    public ProductDetailsData getProduct() {
        return product;
    }

    public void setProduct(ProductDetailsData product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public void setSubtotal(CartItemDetailsData cartItemDetailsData) {
        this.subtotal = cartItemDetailsData.getPrice() * cartItemDetailsData.getCartQuantity();
    }
}
