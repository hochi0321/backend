package com.fsse2203.project_backend.repository;

import com.fsse2203.project_backend.data.cartItem.entity.CartItemEntity;
import com.fsse2203.project_backend.data.user.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CartRepository extends CrudRepository<CartItemEntity, Integer> {
    List<CartItemEntity> findByUser_Uid(Integer uid);
    CartItemEntity findCartItemEntityByProduct_PidAndUser_Uid(Integer pid, Integer uid);
    List<CartItemEntity> findAllByUser(UserEntity userEntity);
    void deleteByUser_Uid(Integer uid);
}
