package com.fsse2203.project_backend.repository;

import com.fsse2203.project_backend.data.cartItem.entity.CartItemEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CartSQLRepository extends CrudRepository<CartItemEntity, Integer> {
    @Query("SELECT c FROM CartItemEntity c WHERE c.user.uid = ?1")
    List<CartItemEntity> finduid(Integer uid);
    @Query("SELECT c FROM CartItemEntity c WHERE c.product.pid = ?1 AND c.user.uid = ?2")
    CartItemEntity findCartitem(Integer pid, Integer uid);
    @Query("DELETE FROM CartItemEntity c WHERE c.user.uid = ?1")
    void deleteUser(Integer uid);
}
