package com.fsse2203.project_backend.repository;

import com.fsse2203.project_backend.data.product.entity.ProductEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<ProductEntity, Integer> {
    @Query("SELECT p FROM ProductEntity p WHERE p.pid = ?1")
    ProductEntity findProductEntityByPid(Integer pid);
}
