package com.fsse2203.project_backend.repository;

import com.fsse2203.project_backend.data.transaction.entity.TransactionEntity;
import com.fsse2203.project_backend.data.transactionProduct.entity.TransactionProductEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionProductRepository extends CrudRepository<TransactionProductEntity, Integer> {
    @Query("SELECT t FROM TransactionProductEntity t WHERE t.transaction = ?1")
    List<TransactionProductEntity> findByTransaction(TransactionEntity entity);
}
