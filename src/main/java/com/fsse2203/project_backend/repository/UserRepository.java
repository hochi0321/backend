package com.fsse2203.project_backend.repository;

import com.fsse2203.project_backend.data.user.entity.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends CrudRepository<UserEntity, Integer> {
    @Query("SELECT u FROM UserEntity u WHERE u.firebaseUid = ?1")
    UserEntity findByFirebaseUid(@Param("firebaseUid") String firebaseUid);
}
