package com.fsse2203.project_backend.service;

import com.fsse2203.project_backend.Exception.CartItemNotFoundException;
import com.fsse2203.project_backend.Exception.InvalidQuantityException;
import com.fsse2203.project_backend.Exception.ProductNotFoundException;
import com.fsse2203.project_backend.Exception.UserNotFoundException;
import com.fsse2203.project_backend.data.cartItem.*;
import com.fsse2203.project_backend.data.cartItem.entity.CartItemEntity;
import com.fsse2203.project_backend.data.user.entity.UserEntity;

import java.util.List;

public interface CartService {
    CartItemResultData addCartItem(AddCartItemData data) throws ProductNotFoundException, InvalidQuantityException, UserNotFoundException;
    List<CartItemDetailsData> getAllCartItem(String firebaseUid) throws UserNotFoundException;
    CartItemDetailsData updateCartQuantity(UpdateCartQuantityData data) throws CartItemNotFoundException, InvalidQuantityException, UserNotFoundException;
    CartItemResultData deleteCartItem(DeleteCartItemData data) throws CartItemNotFoundException, UserNotFoundException;
    void clearUserCart(Integer uid);
    List<CartItemEntity> getAllCartItemEntitiesByUserEntity(UserEntity userEntity);
}
