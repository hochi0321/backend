package com.fsse2203.project_backend.service;

import com.fsse2203.project_backend.Exception.StockNotEnoughException;
import com.fsse2203.project_backend.Exception.ProductNotFoundException;
import com.fsse2203.project_backend.data.product.CreateProductData;
import com.fsse2203.project_backend.data.product.ProductDetailsData;
import com.fsse2203.project_backend.data.product.entity.ProductEntity;

import java.util.List;

public interface ProductService {
    List<ProductDetailsData> getAllProduct();

    ProductDetailsData getProductDetailsDataById(Integer pid) throws ProductNotFoundException;

    ProductEntity getProductEntityById(Integer pid) throws ProductNotFoundException;

    ProductDetailsData createProduct(CreateProductData data);

    void deductProductStock(Integer pid, Integer deduction) throws ProductNotFoundException, StockNotEnoughException;
}
