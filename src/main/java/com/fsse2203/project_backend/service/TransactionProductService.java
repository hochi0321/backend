package com.fsse2203.project_backend.service;

import com.fsse2203.project_backend.Exception.CartItemNotFoundException;
import com.fsse2203.project_backend.Exception.TransactionNotFoundException;
import com.fsse2203.project_backend.data.cartItem.entity.CartItemEntity;
import com.fsse2203.project_backend.data.transaction.entity.TransactionEntity;
import com.fsse2203.project_backend.data.transactionProduct.entity.TransactionProductEntity;


public interface TransactionProductService {
    TransactionProductEntity createTransactionProduct(CartItemEntity cartItemEntity, TransactionEntity transactionEntity) throws CartItemNotFoundException, TransactionNotFoundException;
}
