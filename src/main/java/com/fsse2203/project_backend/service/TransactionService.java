package com.fsse2203.project_backend.service;

import com.fsse2203.project_backend.Exception.*;
import com.fsse2203.project_backend.data.transaction.TransactionDetailsData;

public interface TransactionService {
    TransactionDetailsData createTransaction(String firebaseUid) throws ProductNotFoundException, UserNotFoundException, CartEmptyException, CartItemNotFoundException, TransactionNotFoundException;

    TransactionDetailsData getTransactionDetailsByTid(Integer tid,String firebaseUid) throws ProductNotFoundException, TransactionNotFoundException;

    TransactionDetailsData updateTransactionStatus(Integer tid,String firebaseUid) throws ProductNotFoundException, TransactionNotFoundException, StockNotEnoughException;

    TransactionDetailsData finishTransaction(Integer tid,String firebaseUid) throws ProductNotFoundException, TransactionNotFoundException;
}
