package com.fsse2203.project_backend.service;

import com.fsse2203.project_backend.data.user.CreateUserData;
import com.fsse2203.project_backend.data.user.UserDetailsData;
import com.fsse2203.project_backend.data.user.entity.UserEntity;

public interface UserService {
    UserDetailsData getUserDetailsDataByFirebaseUid(String firebaseUid);
    UserEntity getUserEntityByFirebaseUid(String firebaseUid);
    void createUser(CreateUserData data);
}
