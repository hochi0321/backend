package com.fsse2203.project_backend.service.impl;

import com.fsse2203.project_backend.Exception.CartItemNotFoundException;
import com.fsse2203.project_backend.Exception.InvalidQuantityException;
import com.fsse2203.project_backend.Exception.ProductNotFoundException;
import com.fsse2203.project_backend.Exception.UserNotFoundException;
import com.fsse2203.project_backend.data.cartItem.*;
import com.fsse2203.project_backend.data.cartItem.entity.CartItemEntity;
import com.fsse2203.project_backend.data.product.entity.ProductEntity;
import com.fsse2203.project_backend.data.user.entity.UserEntity;
import com.fsse2203.project_backend.repository.CartRepository;
import com.fsse2203.project_backend.repository.CartSQLRepository;
import com.fsse2203.project_backend.service.CartService;
import com.fsse2203.project_backend.service.ProductService;
import com.fsse2203.project_backend.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
public class CartServiceImpl implements CartService {
    Logger logger = LoggerFactory.getLogger(CartServiceImpl.class);

    private final CartRepository cartRepository;
    private final CartSQLRepository cartSQLRepository;
    private final UserService userService;
    private final ProductService productService;

    @Autowired
    public CartServiceImpl(CartRepository cartRepository, CartSQLRepository cartSQLRepository, UserService userService, ProductService productService) {
        this.cartRepository = cartRepository;
        this.cartSQLRepository = cartSQLRepository;
        this.userService = userService;
        this.productService = productService;
    }

    @Override
    public CartItemResultData addCartItem(AddCartItemData data) throws ProductNotFoundException, InvalidQuantityException, UserNotFoundException {
        UserEntity userEntity = userService.getUserEntityByFirebaseUid(data.getFirebaseUid());
        if (userEntity == null) {
            logger.warn("Add Cart Item: User Not Found");
            throw new UserNotFoundException();
        }
        ProductEntity productEntity = productService.getProductEntityById(data.getPid());
        CartItemEntity cartItemEntity = new CartItemEntity(productEntity, userEntity, data.getQuantity());
        CartItemEntity existingCartItemEntity = cartSQLRepository.findCartitem(productEntity.getPid(), userEntity.getUid());
        if (existingCartItemEntity != null) {
            existingCartItemEntity.setQuantity(existingCartItemEntity.getQuantity() + data.getQuantity());
            cartItemEntity = existingCartItemEntity;
        }
        if (cartItemEntity.getQuantity() <= 0 || cartItemEntity.getQuantity() > productEntity.getStock()) {
            logger.warn("Add Cart Item: Not enough stock");
            throw new InvalidQuantityException();
        }
        cartRepository.save(cartItemEntity);
        logger.info("Added Cart Item, cid: " + cartItemEntity.getCid());
        return new CartItemResultData();
    }

    @Override
    public List<CartItemDetailsData> getAllCartItem(String firebaseUid) throws UserNotFoundException {
        UserEntity userEntity = userService.getUserEntityByFirebaseUid(firebaseUid);
        if (userEntity == null) {
            logger.warn("Get All Cart Item: User Not Found");
            throw new UserNotFoundException();
        }
        List<CartItemDetailsData> datas = new ArrayList<>();
        for (CartItemEntity cartItemEntity : cartSQLRepository.finduid(userEntity.getUid())) {
            datas.add(new CartItemDetailsData(cartItemEntity));
        }
        return datas;
    }

    @Override
    public CartItemDetailsData updateCartQuantity(UpdateCartQuantityData data) throws CartItemNotFoundException, InvalidQuantityException, UserNotFoundException {
        UserEntity userEntity = userService.getUserEntityByFirebaseUid(data.getFirebaseUid());
        if (userEntity == null) {
            logger.warn("Update Cart Item Quantity: User Not Found");
            throw new UserNotFoundException();
        }
        CartItemEntity entity = cartSQLRepository.findCartitem(data.getPid(), userEntity.getUid());
        if (entity == null) {
            logger.warn("Update Cart Item Quantity: Cart Item Not Found");
            throw new CartItemNotFoundException();
        }
        if (data.getQuantity() <= 0 || data.getQuantity() > entity.getProduct().getStock()) {
            logger.warn("Update Cart Item Quantity: Not enough stock");
            throw new InvalidQuantityException();
        }
        entity.setQuantity(data.getQuantity());
        logger.info("Updated Cart Item Quantity, cid: " + entity.getCid());
        return new CartItemDetailsData(cartRepository.save(entity));
    }

    @Override
    public CartItemResultData deleteCartItem(DeleteCartItemData data) throws CartItemNotFoundException, UserNotFoundException {
        UserEntity userEntity = userService.getUserEntityByFirebaseUid(data.getFirebaseUid());
        if (userEntity == null) {
            logger.warn("Delete Cart Item: User Not Found");
            throw new UserNotFoundException();
        }
        CartItemEntity cartItemEntity = cartSQLRepository.findCartitem(data.getPid(), userEntity.getUid());
        if (cartItemEntity == null) {
            logger.warn("Delete Cart Item: Cart Item Not Found");
            throw new CartItemNotFoundException();
        }
        logger.info("Deleted Cart Item, cid: " + cartItemEntity.getCid());
        cartRepository.delete(cartItemEntity);
        return new CartItemResultData();
    }

    @Transactional
    public void clearUserCart(Integer uid) {
        cartRepository.deleteByUser_Uid(uid);
        logger.info("Cleared User Cart, uid: " + uid);
    }

    @Override
    public List<CartItemEntity> getAllCartItemEntitiesByUserEntity(UserEntity userEntity) {
        return cartRepository.findAllByUser(userEntity);
    }
}
