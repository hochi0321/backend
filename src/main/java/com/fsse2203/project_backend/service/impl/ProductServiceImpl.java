package com.fsse2203.project_backend.service.impl;

import com.fsse2203.project_backend.Exception.StockNotEnoughException;
import com.fsse2203.project_backend.Exception.ProductNotFoundException;
import com.fsse2203.project_backend.data.product.CreateProductData;
import com.fsse2203.project_backend.data.product.ProductDetailsData;
import com.fsse2203.project_backend.data.product.entity.ProductEntity;
import com.fsse2203.project_backend.repository.ProductRepository;
import com.fsse2203.project_backend.service.ProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    Logger logger = LoggerFactory.getLogger(ProductServiceImpl.class);
    private final ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<ProductDetailsData> getAllProduct() {
        List<ProductDetailsData> productDatas = new ArrayList<>();
        for (ProductEntity entity : productRepository.findAll()) {
            productDatas.add(new ProductDetailsData(entity));
        }
        return productDatas;
    }

    @Override
    public ProductDetailsData getProductDetailsDataById(Integer pid) throws ProductNotFoundException {
        ProductEntity productEntity = productRepository.findProductEntityByPid(pid);
        if (productEntity == null) {
            throw new ProductNotFoundException();
        }
        return new ProductDetailsData(productEntity);
    }

    @Override
    public ProductEntity getProductEntityById(Integer pid) throws ProductNotFoundException {
        ProductEntity productEntity = productRepository.findProductEntityByPid(pid);
        if (productEntity == null) {
            throw new ProductNotFoundException();
        }
        return productEntity;
    }

    @Override
    public ProductDetailsData createProduct(CreateProductData data) {
        ProductEntity productEntity = productRepository.save(new ProductEntity(data));
        logger.info("Created Product, pid: " + productEntity.getPid());
        return new ProductDetailsData(productEntity);
    }

    @Override
    public void deductProductStock(Integer pid, Integer deduction) throws ProductNotFoundException, StockNotEnoughException {
        ProductEntity productEntity = productRepository.findProductEntityByPid(pid);
        if (productEntity == null) {
            logger.warn("Deduct Product Stock: Cart Item Not Found");
            throw new ProductNotFoundException();
        }
        if (productEntity.getStock() < deduction) {
            logger.warn("Deduct Product Stock: Not Enough Stock");
            throw new StockNotEnoughException();
        }
        System.out.println(productEntity.getStock());
        productEntity.setStock(productEntity.getStock() - deduction);
        productRepository.save(productEntity);
        logger.info("Deducted Product Stock, pid: " + productEntity.getPid() + " number: " + deduction + ", left " + productEntity.getStock());
    }
}
