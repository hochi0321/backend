package com.fsse2203.project_backend.service.impl;

import com.fsse2203.project_backend.Exception.CartItemNotFoundException;
import com.fsse2203.project_backend.Exception.TransactionNotFoundException;
import com.fsse2203.project_backend.data.cartItem.entity.CartItemEntity;
import com.fsse2203.project_backend.data.transaction.entity.TransactionEntity;
import com.fsse2203.project_backend.data.transactionProduct.entity.TransactionProductEntity;
import com.fsse2203.project_backend.repository.TransactionProductRepository;
import com.fsse2203.project_backend.service.TransactionProductService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TransactionProductServiceImpl implements TransactionProductService {
    Logger logger = LoggerFactory.getLogger(TransactionProductServiceImpl.class);
    private final TransactionProductRepository transactionProductRepository;

    @Autowired
    public TransactionProductServiceImpl(TransactionProductRepository transactionProductRepository) {
        this.transactionProductRepository = transactionProductRepository;
    }

    public TransactionProductEntity createTransactionProduct(CartItemEntity cartItemEntity, TransactionEntity transactionEntity) throws CartItemNotFoundException, TransactionNotFoundException {
        if (cartItemEntity == null) {
            logger.warn("Create Transaction Product: cartItemEntity is null");
            throw new CartItemNotFoundException();
        }
        if (transactionEntity == null){
            logger.warn("Create Transaction Product: transactionEntity is null");
            throw new TransactionNotFoundException();
        }
        return transactionProductRepository.save(new TransactionProductEntity(cartItemEntity,transactionEntity));
    }
}
