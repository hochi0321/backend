package com.fsse2203.project_backend.service.impl;

import com.fsse2203.project_backend.Exception.*;
import com.fsse2203.project_backend.data.cartItem.entity.CartItemEntity;
import com.fsse2203.project_backend.data.transaction.Status;
import com.fsse2203.project_backend.data.transaction.TransactionDetailsData;
import com.fsse2203.project_backend.data.transaction.entity.TransactionEntity;
import com.fsse2203.project_backend.data.transactionProduct.entity.TransactionProductEntity;
import com.fsse2203.project_backend.data.user.entity.UserEntity;
import com.fsse2203.project_backend.repository.TransactionRepository;
import com.fsse2203.project_backend.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionServiceImpl implements TransactionService {
    Logger logger = LoggerFactory.getLogger(TransactionServiceImpl.class);
    private final CartService cartService;
    private final UserService userService;
    private final ProductService productService;
    private final TransactionRepository transactionRepository;
    private final TransactionProductService transactionProductService;

    @Autowired
    public TransactionServiceImpl(CartService cartService, UserService userService, ProductService productService, TransactionRepository transactionRepository, TransactionProductService transactionProductService) {
        this.cartService = cartService;
        this.userService = userService;
        this.productService = productService;
        this.transactionRepository = transactionRepository;
        this.transactionProductService = transactionProductService;
    }

    @Override
    public TransactionDetailsData createTransaction(String firebaseUid) throws UserNotFoundException, CartEmptyException, CartItemNotFoundException, TransactionNotFoundException {
        UserEntity userEntity = userService.getUserEntityByFirebaseUid(firebaseUid);
        List<CartItemEntity> cartItemEntities = cartService.getAllCartItemEntitiesByUserEntity(userEntity);
        if (userEntity == null) {
            logger.warn("Create Transaction: User Not Found");
            throw new UserNotFoundException();
        }
        if (cartItemEntities == null || cartItemEntities.isEmpty()) {
            logger.warn("Create Transaction: Cart is Empty");
            throw new CartEmptyException();
        }
        TransactionEntity transactionEntity = transactionRepository.save(new TransactionEntity(userEntity, cartItemEntities));
        List<TransactionProductEntity> transactionProductEntities = new ArrayList<>();
        for (CartItemEntity cartItemEntity : cartItemEntities) {
            transactionProductEntities.add(transactionProductService.createTransactionProduct(cartItemEntity,transactionEntity));
        }
        transactionEntity.setItems(transactionProductEntities);
        logger.info("Created Transaction, tid: " + transactionEntity.getTid());
        return new TransactionDetailsData(transactionEntity);
    }

    public TransactionDetailsData getTransactionDetailsByTid(Integer tid, String firebaseUid) throws TransactionNotFoundException {
        TransactionEntity transactionEntity = transactionRepository.findByTidAndUserFirebaseUid(tid, firebaseUid);
        if (transactionEntity == null) {
            logger.warn("Get Transaction Details: Transaction Not Found");
            throw new TransactionNotFoundException();
        }
        return new TransactionDetailsData(transactionEntity);
    }

    @Override
    public TransactionDetailsData updateTransactionStatus(Integer tid,String firebaseUid) throws ProductNotFoundException, TransactionNotFoundException, StockNotEnoughException {
        TransactionEntity transactionEntity = transactionRepository.findByTidAndUserFirebaseUid(tid, firebaseUid);
        if (transactionEntity == null) {
            logger.warn("Update Transaction Status: Transaction Not Found");
            throw new TransactionNotFoundException();
        }
        transactionEntity.setStatus(Status.PROCESSING);
        for (TransactionProductEntity transactionProductEntity : transactionEntity.getItems()) {
            productService.deductProductStock(transactionProductEntity.getPid(), transactionProductEntity.getQuantity());
        }
        logger.info("Updated Transaction Status, tid: " + transactionEntity.getTid());
        return new TransactionDetailsData(transactionRepository.save(transactionEntity));
    }

    @Override
    public TransactionDetailsData finishTransaction(Integer tid,String firebaseUid) throws TransactionNotFoundException {
        TransactionEntity transactionEntity = transactionRepository.findByTidAndUserFirebaseUid(tid, firebaseUid);
        if (transactionEntity == null) {
            logger.warn("Finish Transaction: Transaction Not Found");
            throw new TransactionNotFoundException();
        }
        transactionEntity.setStatus(Status.SUCCESS);
        logger.info("Finished Transaction, tid: " + transactionEntity.getTid());
        cartService.clearUserCart(transactionEntity.getUser().getUid());
        return new TransactionDetailsData(transactionRepository.save(transactionEntity));
    }
}
