package com.fsse2203.project_backend.service.impl;

import com.fsse2203.project_backend.data.user.CreateUserData;
import com.fsse2203.project_backend.data.user.UserDetailsData;
import com.fsse2203.project_backend.data.user.entity.UserEntity;
import com.fsse2203.project_backend.repository.UserRepository;
import com.fsse2203.project_backend.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void createUser(CreateUserData data) {
        UserEntity entity = new UserEntity(data);
        userRepository.save(entity);
    }

    @Override
    public UserDetailsData getUserDetailsDataByFirebaseUid(String firebaseUid) {
        UserEntity entity = getUserEntityByFirebaseUid(firebaseUid);
        if (entity==null){
            return null;
        }
        return new UserDetailsData(entity);
    }

    public UserEntity getUserEntityByFirebaseUid(String firebaseUid) {
        return userRepository.findByFirebaseUid(firebaseUid);
    }
}
