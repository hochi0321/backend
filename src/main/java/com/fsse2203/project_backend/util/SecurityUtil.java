package com.fsse2203.project_backend.util;

import com.fsse2203.project_backend.data.user.UserDetailsData;
import com.fsse2203.project_backend.service.UserService;
import com.google.firebase.auth.FirebaseToken;
import org.springframework.security.core.Authentication;

public class SecurityUtil {
    public static String getFirebaseUid(Authentication authentication) {
        FirebaseToken token = (FirebaseToken)authentication.getPrincipal();
        return token.getUid();
    }
}
